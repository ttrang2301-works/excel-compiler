import java.util.regex.Pattern;

/**
 * Copyright (c) 2019 Absolute Software Corporation. All rights reserved. Reproduction or
 * transmission in whole or in part, in any form or by any means, electronic, mechanical or
 * otherwise, is prohibited without the prior written consent of the copyright owner.
 */

public class Number extends Token {

    // TODO
    public static final String REGEX_NUMBER = "PATTERN_CELL_EXPRESSION_NUMBER";
    public static final Pattern PATTERN_NUMBER = Pattern.compile(REGEX_NUMBER);
}
