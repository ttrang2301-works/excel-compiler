import java.util.regex.Pattern;

/**
 * Copyright (c) 2019 Absolute Software Corporation. All rights reserved. Reproduction or
 * transmission in whole or in part, in any form or by any means, electronic, mechanical or
 * otherwise, is prohibited without the prior written consent of the copyright owner.
 */

public class CellRef extends Token {

    // TODO
    public static final String REGEX_CELLREF = "PATTERN_CELL_EXPRESSION_FORMULA_TOKEN_CELLREF";
    public static final Pattern PATTERN_CELLREF = Pattern.compile(REGEX_CELLREF);
}
