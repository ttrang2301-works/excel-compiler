import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Application {

    public static final String REGEX_FORMULA = "PATTERN_CELL_EXPRESSION_FORMULA";
    private static final Pattern PATTERN_FORMULA = Pattern.compile(REGEX_FORMULA);

    private Map<String, Tree> cells = new HashMap<>();
    private Map<String, Integer> result = new HashMap<>();

    public static void main(String[] args) {
        String fileName = ".\\test1.txt";
        System.out.println("Processing " + fileName);
        new Application().doIt(fileName);
    }

    private void doIt(String fileName) {
        buildDataStruture(fileName);
        // TODO detect issue
        // TODO order expressions by topology
        // TODO Calculate expressions
    }

    private void buildDataStruture(String fileName) {
        try (Scanner scanner = new Scanner(new File(fileName))) {
            if (!scanner.hasNext()) {
                throw new Exception("File is empty");
            }
            scanner.nextLine();
            while (scanner.hasNext()) {
                String cellName = scanner.nextLine();
                if (!CellRef.PATTERN_CELLREF.matcher(cellName).matches()) {
                    throw new Exception("Invalid cell name: " + cellName);
                }
                if (!scanner.hasNext()) {
                    throw new Exception("Invalid format: no value for cell " + cellName);
                }
                String cellExpression = scanner.nextLine();
                if (!Number.PATTERN_NUMBER.matcher(cellExpression).matches()
                        || !PATTERN_FORMULA.matcher(cellExpression).matches()) {
                    throw new Exception("Invalid cell expression: " + cellExpression);
                }
                if (Number.PATTERN_NUMBER.matcher(cellExpression).matches()) {
                    result.put(cellName, Integer.valueOf(cellExpression));
                }
                cells.put(cellName, toTree(cellExpression));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Tree toTree(String cellExpression) {
        cellExpression = cellExpression.replace(" ", "");
        List<Token> tokens = new ArrayList<>();
        // TODO
        return null;
    }

}
