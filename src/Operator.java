import java.util.regex.Pattern;

/**
 * Copyright (c) 2019 Absolute Software Corporation. All rights reserved. Reproduction or
 * transmission in whole or in part, in any form or by any means, electronic, mechanical or
 * otherwise, is prohibited without the prior written consent of the copyright owner.
 */

public class Operator extends Token {

    // TODO
    public static final String REGEX_OPERATOR = "PATTERN_CELL_EXPRESSION_FORMULA_TOKEN_OPERATOR";
    public static final Pattern PATTERN_OPERATOR = Pattern.compile(REGEX_OPERATOR);
}
